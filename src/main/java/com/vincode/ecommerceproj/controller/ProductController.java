package com.vincode.ecommerceproj.controller;

import com.vincode.ecommerceproj.model.Product;
import com.vincode.ecommerceproj.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping("/")
    public ResponseEntity<Object> createItems(@RequestBody Product product){
        productService.addItems(product);
        return new ResponseEntity<>("Created!", HttpStatus.CREATED);
    }

    @GetMapping("/")
    public ResponseEntity<Object> getItems(){
        return new ResponseEntity<>(productService.getAllItems(), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateItem(@PathVariable Long id, @RequestBody Product product){
        productService.updateItem(id, product);
        return new ResponseEntity<>("Updated!", HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteItem(@PathVariable Long id){
        productService.deleteItem(id);
        return new ResponseEntity<>("Deleted!", HttpStatus.OK);
    }
}

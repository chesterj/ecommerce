package com.vincode.ecommerceproj.controller;

import com.vincode.ecommerceproj.model.User;
import com.vincode.ecommerceproj.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/")
    public ResponseEntity<Object> creatingUser(@RequestBody User user){
        userService.createUser(user);
        return new ResponseEntity<>("Created!", HttpStatus.CREATED);
    }

    @GetMapping("/")
    public ResponseEntity<Object> getUsers(){
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    @PutMapping("/{userid}")
    public ResponseEntity<Object> updateUser(@PathVariable Long userid, @RequestBody User user){
        userService.updateUser(userid, user);
        return new ResponseEntity<>("Updated!", HttpStatus.OK);
    }

    @DeleteMapping("/{userid}")
    public ResponseEntity<Object> deleteUser(@PathVariable Long userid){
        userService.deleteUser(userid);
        return new ResponseEntity<>("Deleted!", HttpStatus.OK);
    }
}

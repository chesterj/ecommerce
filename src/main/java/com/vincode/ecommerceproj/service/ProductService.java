package com.vincode.ecommerceproj.service;

import com.vincode.ecommerceproj.model.Product;

public interface ProductService {

    void addItems(Product product);
    Iterable<Product> getAllItems();
    void updateItem(Long id, Product product);
    void deleteItem(Long id);
}

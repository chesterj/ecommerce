package com.vincode.ecommerceproj.service;

import com.vincode.ecommerceproj.model.User;

public interface UserService {

    void createUser(User user);
    Iterable<User> getUsers();
    void updateUser(Long id, User user);
    void deleteUser(Long id);
}
